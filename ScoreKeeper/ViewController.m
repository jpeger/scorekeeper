//
//  ViewController.m
//  ScoreKeeper
//
//  Created by Stephen Bromley on 1/14/15.
//  Copyright (c) 2015 Stephen Bromley. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    int player1score = 0;
    int player2score = 0;
    
    UITextField* player1text =[[UITextField alloc]initWithFrame:CGRectMake(self.view.frame.size.width/4 -50, 0, 100, 100)];
    [player1text setPlaceholder:@"Player 1 Name"];
    [player1text setTextAlignment:NSTextAlignmentCenter];
    [player1text setDelegate:self];
    [self.view addSubview:player1text];

    player1Lbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/4 -50, 100, 100, 100)];
    
    player2Lbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width-player1Lbl.frame.size.width-50, 100, 100, 100)];
    
    player1Lbl.text = [NSString stringWithFormat:@"%d",player1score];
    player1Lbl.font = [UIFont systemFontOfSize:100];
    [self.view addSubview:player1Lbl];
    
    player2Lbl.text = [NSString stringWithFormat:@"%d",player2score];
    player2Lbl.font = [UIFont systemFontOfSize:100];
    [self.view addSubview:player2Lbl];
    
    player1stepper = [[UIStepper alloc]initWithFrame:CGRectMake(player1Lbl.frame.origin.x, player1Lbl.frame.origin.y + player1Lbl.frame.size.height + 100, player1Lbl.frame.size.width, 50)];
    
    [player1stepper addTarget:self action:@selector(player1stepperTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    UITextField* player2text =[[UITextField alloc]initWithFrame:CGRectMake(self.view.frame.size.width-player1Lbl.frame.size.width-50, 0, 100, 100)];
    [player2text setPlaceholder:@"Player 2 Name"];
    [player2text setTextAlignment:NSTextAlignmentCenter];
    [player2text setDelegate:self];
    [self.view addSubview:player2text];
    
    player2stepper = [[UIStepper alloc]initWithFrame:CGRectMake(player2Lbl.frame.origin.x, player2Lbl.frame.origin.y+player2Lbl.frame.size.height+100, player2Lbl.frame.size.width, 50)];
    
    [player2stepper addTarget:self action:@selector(player2stepperTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:player1stepper];
    [self.view addSubview:player2stepper];
    
    UIButton* reset = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [reset setFrame:CGRectMake(self.view.frame.size.width/3, 400, self.view.frame.size.width/3, 50)];
    [reset setTitle:@"Reset" forState:UIControlStateNormal];
    [reset addTarget:self action:@selector(resetScores:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:reset];
}

-(void)player1stepperTouched:(UIStepper*)playerstepper{
    player1Lbl.text = [NSString stringWithFormat:@"%.f",playerstepper.value];
}

-(void)player2stepperTouched:(UIStepper*)playerstepper{
    player2Lbl.text = [NSString stringWithFormat:@"%.f",playerstepper.value];
}

-(void)resetScores:(id)sender{
    player1stepper.value = 0.0;
    player2stepper.value = 0.0;
    player1Lbl.text = [NSString stringWithFormat:@"%d",0];
    player2Lbl.text = [NSString stringWithFormat:@"%d",0];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
