//
//  ViewController.h
//  ScoreKeeper
//
//  Created by Stephen Bromley on 1/14/15.
//  Copyright (c) 2015 Stephen Bromley. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate>

{
    UILabel* player1Lbl;
    UILabel* player2Lbl;
    UIStepper* player1stepper;
    UIStepper* player2stepper;
}

@end

